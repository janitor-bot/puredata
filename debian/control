Source: puredata
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Paul Brossier <piem@debian.org>,
 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 gettext,
 libasound2-dev [linux-any],
 libjack-dev,
 portaudio19-dev,
Build-Depends-Indep:
 python3:any,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://puredata.info
Vcs-Git: https://salsa.debian.org/multimedia-team/pd/puredata.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/pd/puredata

Package: puredata
Architecture: all
Depends:
 puredata-core  (<< ${source:Upstream-Version}+1~),
 puredata-core  (>= ${source:Version}),
 puredata-dev   (<< ${source:Upstream-Version}+1~),
 puredata-dev   (>= ${source:Version}),
 puredata-doc   (<< ${source:Upstream-Version}+1~),
 puredata-doc   (>= ${source:Version}),
 puredata-extra (<< ${source:Upstream-Version}+1~),
 puredata-extra (>= ${source:Version}),
 puredata-gui   (<< ${source:Upstream-Version}+1~),
 puredata-gui   (>= ${source:Version}),
 puredata-utils,
 ${misc:Depends},
Suggests:
 gem,
 multimedia-puredata,
 pd-aubio,
 pd-csound,
 pd-pdp,
 pd-zexy,
Description: realtime computer music and graphics system
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing. Pd's audio functions are built-in;
 graphical computations require separate packages such as gem (Graphics
 Environment for Multimedia) or pd-pdp (Pd Packet).
 .
 This is a metapackage that depends on all components of the core of Pd.

Package: puredata-core
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 puredata-gui,
Provides:
 pd,
Breaks:
 puredata-gui (<< ${source:Upstream-Version}),
 puredata-gui (>> ${source:Upstream-Version}+1~),
Description: realtime computer music and graphics system - core components
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package only provides the core infrastructure of Pure Data.
 Most likely you will want to install "puredata-gui" (or even "puredata")
 as well.

Package: puredata-gui
Architecture: all
Multi-Arch: foreign
Depends:
 python3:any,
 tcl,
 tk,
 ${misc:Depends},
Recommends:
 puredata,
 puredata-gui-l10n,
 sensible-utils,
Breaks:
 puredata-core (<< ${source:Upstream-Version}),
 puredata-core (>> ${source:Upstream-Version}+1~),
Description: realtime computer music and graphics system - GUI
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides the graphical user-interface for Pure Data.
 Most likely you will want to install "puredata-core" (or even "puredata")
 as well.
 Installing this package without the accompanying puredata-core is only useful
 if you want to run the GUI and the DSP on different machines.

Package: puredata-gui-l10n
Architecture: all
Multi-Arch: foreign
Section: localization
Depends:
 puredata-gui (= ${source:Version}),
 ${misc:Depends},
Description: realtime computer music and graphics system - translations
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides the translations for the graphical user-interface of
 Pure Data.

Package: puredata-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
Recommends:
 node-html5shiv,
 puredata,
Description: realtime computer music and graphics system - documentation
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides the documentation for Pure Data.
 Most likely you will want to install "puredata" as well.

Package: puredata-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: realtime computer music and graphics system - development files
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides the header-files for compiling externals (plugins) for
 puredata.

Package: puredata-utils
Section: utils
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 pd-utils,
Description: realtime computer music and graphics system - utility programs
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides utility applications for puredata, namely pdsend and
 pdreceive, for sending and receiving FUDI over the net.

Package: puredata-extra
Architecture: any
Depends:
 puredata-core (<< ${source:Upstream-Version}+1~),
 puredata-core (>= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: realtime computer music and graphics system - extra files
 Pure Data (also known as Pd) is a real-time graphical programming environment
 for audio and graphics processing.
 .
 This package provides extra objects that come with Pd, e.g. for signal
 analysis (fiddle~, sigmund~, bonk~) and more.
 .
 The objects for expression evaluation (expr~) have been included into
 puredata-core, so they are no longer part of this package.
